<?php
  require_once("devCorpDashboard/connect.php");
  $reponse = $bdd->query('SELECT *  FROM article ORDER BY created_at DESC LIMIT 0,2');
?>
<!doctype html>
<html class="no-js" lang="fr">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="DEV CORP" />
<!-- Document Title -->
<title>DEV CORP | Digital Agency</title>

<!-- Favicon -->
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">

<!-- FontsOnline -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Alegreya+Sans:400,500,700,800,900,300,100' rel='stylesheet' type='text/css'>

<!-- StyleSheets -->
<link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<link rel="stylesheet" href="css/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/responsive.css">

<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
<link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />

<!-- JavaScripts -->
<script src="js/vendors/modernizr.js"></script>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-89681610-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- LOADER ===========================================-->
<div id="loader">
  <div class="loader">
    <div class="position-center-center">
      <div id="preloader6"> <span></span> <span></span> <span></span> <span></span> </div>
    </div>
  </div>
</div>

<!-- Page Wrapper -->
<div id="wrap"> 
  
  <!-- Top bar -->
  <div class="container">
    <div class="row">
      <div class="col-md-2 noo-res"></div>
      <div class="col-md-10">
        <div class="top-bar">
          <div class="col-md-3">
            <ul class="social_icons">
              <li><a href="https://www.facebook.com/Digital.DevCorp" target="_blank"><i class="fa fa-facebook"></i></a></li>
              <li><a href="https://twitter.com/DevC4ASM" target="_blank"><i class="fa fa-twitter"></i></a></li>
              <li><a href="https://plus.google.com/u/0/+DevCorpFourASM" target="_blank"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="https://www.linkedin.com/in/devcorp/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
            </ul>
          </div>
          
          <!-- Social Icon -->
          <div class="col-md-9">
            <ul class="some-info font-montserrat">
              <li><i class="fa fa-phone"></i> +221 76 660 30 10</li>
              <li><i class="fa fa-envelope"></i> contact@devcorptechnologies.com</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Header -->
  <header class="header coporate-header">
    <div class="sticky">
      <div class="container">
        
        <!-- Nav -->
        <nav>
        <div class="logo"> <a href="index.php"><img src="images/logo.png" alt=""></a> </div>
          <ul id="ownmenu" class="ownmenu">
            <li><a href="#about"> THE OFFICE </a></li>
            <li><a href="#services"> SERVICES </a></li>
            <li><a href="#references"> REFERENCES</a></li>
            <li><a href="#blog"> BLOG </a></li>
            <li><a href="#contact"> CONTACT</a></li>
            
            
          </ul>
        </nav>
      </div>
    </div>
  </header>
  <!-- End Header --> 
  
  <!--======= HOME MAIN SLIDER =========-->
  <section class="home-slider">
    <div class="tp-banner-container">
      <div class="tp-banner-fix">
        <ul>
          
          <!-- Slider 1 -->
          <li data-transition="fade" data-slotamount="7"> <img src="images/slides/slide-bg-1.jpg" data-bgposition="center top" alt="" /> 
            
            <!-- Layer -->
            <div class="tp-caption sft tp-resizeme font-extra-bold" 
                  data-x="right" data-hoffset="0"
                  data-y="center" data-voffset="0" 
                  data-speed="700" 
                  data-start="700" 
                  data-easing="easeOutBack"
                  data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-elementdelay="0.1" 
                  data-endelementdelay="0.1" 
                  data-endspeed="300" 
                  data-captionhidden="on"> <img src="images/slides/img--1-1.png" alt="" > </div>
            
            <!-- Layer -->
            <div class="tp-caption sfb tp-resizeme font-bold" 
                  data-x="left" data-hoffset="40"
                  data-y="center" data-voffset="-100"
                  data-speed="500" 
                  data-start="700" 
                  data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
                  data-easing="Back.easeOut" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-elementdelay="0.1" 
                  data-endelementdelay="0.1"
                  data-endspeed="300" 
                  data-captionhidden="on"
                  style="color: #fff; font-size: 48px; font-weight: normal; letter-spacing:0px; line-height:55px;"> Solutions digitales et <br>
              applicatives pour tous </div>
            
            <!-- Layer -->
            <div class="tp-caption sfb tp-resizeme" 
                  data-x="left" data-hoffset="40"
                  data-y="center" data-voffset="30"
                  data-speed="500" 
                  data-start="1000" 
                  data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
                  data-easing="Back.easeOut" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-elementdelay="0.1" 
                  data-endelementdelay="0.1" 
                  data-endspeed="300" 
                  data-captionhidden="on"
                  style="color: #fff; font-size: 30px; font-weight: normal; line-height:36px;"> Bien plus que la fiabilité des nos produits,<br>nous laissons une grande place au design <br>et au User Experience pour laisser à vos <br>utilisateurs le meilleur souvenir possible.</div>
            
            <!-- Layer -->
            <div class="tp-caption sfb tp-resizeme font-crimson" 
                  data-x="left" data-hoffset="40"
                  data-y="center" data-voffset="150"
                  data-speed="500" 
                  data-start="1300" 
                  data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
                  data-easing="Back.easeOut" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-elementdelay="0.1" 
                  data-endelementdelay="0.1" 
                  data-endspeed="300" 
                  data-captionhidden="on"
                  style=""> <a href="#services" class="btn">En savoir plus</a></div>
          </li>
          
          <!-- Slider 1 -->
          <li data-transition="fade" data-slotamount="7"> <img src="images/slides/slide-bg-2.jpg" data-bgposition="center top" alt="" /> 
            
            <!-- Layer -->
            <div class="tp-caption sft tp-resizeme font-extra-bold" 
                  data-x="right" data-hoffset="0"
                  data-y="center" data-voffset="0" 
                  data-speed="700" 
                  data-start="700" 
                  data-easing="easeOutBack"
                  data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-elementdelay="0.1" 
                  data-endelementdelay="0.1" 
                  data-endspeed="300" 
                  data-captionhidden="on" 
                  > <img src="images/slides/img--2-1.png" alt="" > </div>
            
            <!-- Layer -->
            <div class="tp-caption sfb tp-resizeme font-bold" 
                  data-x="left" data-hoffset="40"
                  data-y="center" data-voffset="-100"
                  data-speed="500" 
                  data-start="700" 
                  data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
                  data-easing="Back.easeOut" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-elementdelay="0.1" 
                  data-endelementdelay="0.1"
                  data-endspeed="300" 
                  data-captionhidden="on"
                  style="color: #fff; font-size: 48px; font-weight: normal; letter-spacing:0px; line-height:55px;"> Communication et créativité<br>par la technologie</div>
            
            <!-- Layer -->
            <div class="tp-caption sfb tp-resizeme" 
                  data-x="left" data-hoffset="40"
                  data-y="center" data-voffset="30"
                  data-speed="500" 
                  data-start="1000" 
                  data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
                  data-easing="Back.easeOut" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-elementdelay="0.1" 
                  data-endelementdelay="0.1" 
                  data-endspeed="300" 
                  data-captionhidden="on"
                  style="color: #fff; font-size: 30px; font-weight: normal; line-height:36px;"> Nous analysons, planifions, conversons et<br> faisons preuve de créativité pour répondre <br>à vos besoins et creer une nouvelle relation<br> client-agence plus humaines.</div>

            
            <!-- Layer -->
            <div class="tp-caption sfb tp-resizeme font-crimson" 
                  data-x="left" data-hoffset="40"
                  data-y="center" data-voffset="150"
                  data-speed="500" 
                  data-start="1300" 
                  data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:1;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" 
                  data-easing="Back.easeOut" 
                  data-splitin="none" 
                  data-splitout="none" 
                  data-elementdelay="0.1" 
                  data-endelementdelay="0.1" 
                  data-endspeed="300" 
                  data-captionhidden="on"
                  style=""> <a href="#services" class="btn">En savoir plus</a></div>
          </li>
        </ul>
      </div>
    </div>
  </section>
  
  <!-- Content -->
  <div id="content"> 
    
    <!-- Seo Secore 
    <section class="bg-parallax seo-secore padding-top-100 padding-bottom-100" style="background:url(images/bg/bg-parallax.jpg) no-repeat;">
      <div class="container"> 
        
        
        <div class="heading-block white-text text-center margin-bottom-50">
          <h2>What’s Your SEO Score ?</h2>
          <span>See how well your page is optimised for your keyword</span> </div>
        
        =
        <form>
          <ul class="row">
            <li class="col-md-3">
              <input type="text" class="form-control" placeholder="http://">
            </li>
            <li class="col-md-3">
              <input type="text" class="form-control" placeholder="Keyword">
            </li>
            <li class="col-md-3">
              <input type="text" class="form-control" placeholder="Email">
            </li>
            <li class="col-md-3">
              <button type="submit" class="btn btn-orange">Check Now !</button>
            </li>
          </ul>
        </form>
      </div>
    </section>
    -->
    <!-- Infinity Solution -->
    <section class="light-gray-bg solution padding-top-100 padding-bottom-100" id="about">
      <div class="container"> 
        
        <!-- Tittle -->
        <div class="heading-block text-center margin-bottom-80">
          <h2>Bienvenue </h2>
          <span>Aider et habiliter les personnes grâce à la technologie</span> </div>
        <ul class="row text-center">
          
          <!-- Web Analytics -->
          <li class="col-md-3"> <img src="images/icon-1.png" alt="">
            <h6>Conception et réalisation</h6>
            <p>De toute plateforme web ou de logiciel dont vous avez besoin 
            avec de nouvelles technologies comme ReactJS(Facebook), 
            Ruby on rails et d'autres plus classiques.</p>
          </li>
          
          <!-- Keyword Targeting -->
          <li class="col-md-3"> <img src="images/icon-2.png" alt="">
            <h6>Communication Digital</h6>
            <p>Allant de l'analyse de données au marketing, de la conception 
            de campagne au community management, nous ne laissons rien de coté
            afin de magnifier votre identité virtuelle</p>
          </li>
          
          <!-- Technical Service -->
          <li class="col-md-3"> <img src="images/icon-3.png" alt="">
            <h6>Infographie et illustration 2D/3D</h6>
            <p>Stylo et pinceau n'ont pas de secret pour nous. Des images illustratives et des vidéos captivantes disponibles pour le web et vos écrans.</p>
          </li>
          
          <!-- Support Center -->
          <li class="col-md-3"> <img src="images/icon-4.png" alt="">
            <h6>Audit et consultance</h6>
            <p>Rien ne vaut une bonne révision de l'éfficacité de ses outils de travail et encore plus d'avoir une personne compétente pour vous guider.</p>
          </li>
        </ul>
      </div>
    </section>
    
    <!-- Google Front Page -->
    <section class="front-page padding-top-100 padding-bottom-100">
      <div class="container">
        <div class="row">
          <div class="col-md-6"> <img class="img-responsive margin-top-30" src="images/ipad.jpg" alt=" "> </div>
          <div class="col-md-6"> 
            <!-- Tittle -->
            <div class="heading-block text-left margin-bottom-20">
              <h2>Vous avez déjà votre idée de projet en tête?</h2>
              <p>Rien de plus simple que la concrétiser. Il vous suffit de demander notre cahier des charges pour le remplir vous mêmes ou bien de demander à ce qu'un des membres de l'équipe vous aide à le faire. Nous sommes toujours disponibles pour nos clients.</p>
            </div>
            
            <!-- List Style -->
            <ul class="list-style">
              <li>
                <p><img src="images/list-icon-1.png" alt=""> Nous ancrons notre stratégie autour de votre besoin. </p>
              </li>
              <li>
                <p><img src="images/list-icon-2.png" alt=""> Nous maintenons le contact à tout moment. </p>
              </li>
              <li>
                <p><img src="images/list-icon-3.png" alt=""> Nous utilisons même whatsapp et viber pour communiquer avec vous.</p>
              </li>
              <li>
                <p><img src="images/list-icon-4.png" alt=""> Et nous offrons un des meilleurs services qualité/budget</p>
              </li>
            </ul>
            
            <!-- Buttons --> 
             <a href="#contact" class="btn btn-orange margin-left-100 margin-top-20">Etablir le contact</a> </div>
        </div>
      </div>
    </section>
    
    <!-- Infinity Solution -->
    <section class="offer-services padding-top-100" id="services">
      <div class="container"> 
        
        <!-- Tittle -->
        <div class="heading-block text-center margin-bottom-80">
          <h2>Quels sont les services que nous offrons?</h2>
          <span class="intro-style">Nous sommes une agence aux services digitaux complets. Nos experts en médias sociaux peuvent vous aider à établir vos objectifs d'affaires, à identifier votre public cible, à créer du contenu attrayant et partagé et enfin à intégrer vos médias sociaux à tous les autres aspects de votre présence en ligne.</span> </div>
        <div class="text-center"> <img src="images/services-img.jpg" alt=""> </div>
      </div>
    </section>
    
    <!-- INTRO 
    <section class="bg-parallax text-center padding-top-60 padding-bottom-60" style="background:url(images/bg/bg-parallax.jpg) no-repeat;">
      <div class="container">
        <div class="text-center margin-bottom-50">
          <p class="text-white intro-style font-14px"></p>
        </div>
        <a href="#." class="btn btn-orange">Learn More</a> </div>
    </section>
    -->
    <!-- Case Studies -->
    <section class="case-studies padding-top-100 padding-bottom-100" id="references">
      <div class="container"> 
        
        <!-- Tittle -->
        <div class="heading-block text-center margin-bottom-80">
          <h2>Références </h2>
          <span class="intro-style">Nos dernières réalisations</span> </div>
        
        <!-- Cases -->
        <div class="case">
          <ul class="row">
            
            <!-- Case 1 -->
            <li class="col-md-4">
              <article> <a href="https://www.youtube.com/watch?v=VbQN91adeWk" target="_blank"> <img class="img-responsive" src="images/case-img-1.jpg" alt=""> </a>
                <div class="case-detail">
                  <h5>Projet ONER de la FAO et l'ANPEJ</h5>
                  <p>L'Observatoire National de l'Emploi Rural décent de la FAO et l'ANPEJ</p>
                </div>
              </article>
            </li>
            
            <!-- Case 2 -->
            <li class="col-md-4">
              <article> <a href="https://www.youtube.com/watch?v=RVMpCv4fhSc" target="_blank"> <img class="img-responsive" src="images/case-img-2.jpg" alt=""> </a>
                <div class="case-detail">
                  <h5>Vidéo USAID/SUNUBUDGET</h5>
                  <p>Programme de vidéos animées pour expliquer aux populations le Budget de l'Etat.</p>
                </div>
              </article>
            </li>
            
            <!-- Case 3 -->
            <li class="col-md-4">
              <article> <a href="http://training.concree.com/" target="_blank"> <img class="img-responsive" src="images/case-img-3.jpg" alt=""> </a>
                <div class="case-detail">
                  <h5>Training de CONCREE</h5>
                  <p>Site web vitrine pour les formations données par CONCREE aux entrepreneurs</p>
                </div>
              </article>
            </li>
			<!-- Case 4 -->
            <li class="col-md-4">
              <article> <a href="http://concree.com/fr/" target="_blank"> <img class="img-responsive" src="images/case-img-4.jpg" alt=""> </a>
                <div class="case-detail">
                  <h5>CONCREE</h5>
                  <p>Ajout de modules à la plateforme de mise en relation entre Entrepreneurs, Collaborateurs et Mentors</p>
                </div>
              </article>
            </li>
			<!-- Case 5 -->
            <li class="col-md-4">
              <article> <a href="www.bativerres.sn" target="_blank"> <img class="img-responsive" src="images/case-img-5.jpg" alt=""> </a>
                <div class="case-detail">
                  <h5>Bativerres</h5>
                  <p>Site web vitrine réalisé par notre CTO Mouhamed Gueye pour la société d'aménagement de bureaux et décorations.</p>
                </div>
              </article>
            </li>
			<!-- Case 6 -->
            <li class="col-md-4">
              <article> <a href="#"> <img class="img-responsive" src="images/case-img-6.jpg" alt=""> </a>
                <div class="case-detail">
                  <h5>FO FEETE</h5>
                  <p>Application Facebook pour jauger les tendances lors du référendum 2016 au Sénégal.</p>
                </div>
              </article>
            </li>
          </ul>
          
          <!-- Button -->
          <div class="text-center margin-top-50"> <a href="#contact" class="btn btn-orange">Demander le catalogue</a> </div>
        </div>
      </div>
    </section>
    
    <!-- Case Studies 
    <section class="pricing-table light-gray-bg padding-top-100 padding-bottom-100">
      <div class="container"> 
        
        
        <div class="heading-block text-center margin-bottom-80">
          <h2>Affordable SEO Services Packages </h2>
          <span class="intro-style">Choose from affordable SEO services packages & get the best results in return. </span> </div>
        <div class="row"> 
          
          
          <div class="col-md-4"> 
           
            <div class="plan-icon"><img src="images/plan-icon-1.png" alt=" "></div>
            
         
            <div class="pricing-head">
              <h4>Basic Plan</h4>
              <span class="curency">$</span> <span class="amount">25<span>.99</span></span> <span class="month">/ month</span> </div>
            
            
            <div class="price-in">
              <ul class="text-center">
                <li>25 Analytics Campaigns</li>
                <li> 1,900 Keywords</li>
                <li> 1,250,000 Crawled Pages</li>
                <li> Includes Branded Reports</li>
                <li> 50 Social Accounts</li>
              </ul>
              <a href="#." class="btn btn-orange">PURCHACE</a> </div>
          </div>
          -->
          <!-- 
          <div class="col-md-4"> 
           
            <div class="plan-icon orange-bg"><img src="images/plan-icon-2.png" alt=" "></div>
            
            
            <div class="pricing-head orange-bg">
              <h4>Advanced Plan</h4>
              <span class="curency">$</span> <span class="amount">45<span>.99</span></span> <span class="month">/ month</span> </div>
            
            
            <div class="price-in">
              <ul class="text-center">
                <li>25 Analytics Campaigns</li>
                <li> 1,900 Keywords</li>
                <li> 1,250,000 Crawled Pages</li>
                <li> Includes Branded Reports</li>
                <li> 50 Social Accounts</li>
              </ul>
              <a href="#." class="btn btn-orange">PURCHACE</a> </div>
          </div>
          -->
          <!-- 
          <div class="col-md-4"> 
            
            <div class="plan-icon"><img src="images/plan-icon-3.png" alt=" "></div>
            
            
            <div class="pricing-head">
              <h4>Premium Plan</h4>
              <span class="curency">$</span> <span class="amount">65<span>.99</span></span> <span class="month">/ month</span> </div>
            
            <
            <div class="price-in">
              <ul class="text-center">
                <li>25 Analytics Campaigns</li>
                <li> 1,900 Keywords</li>
                <li> 1,250,000 Crawled Pages</li>
                <li> Includes Branded Reports</li>
                <li> 50 Social Accounts</li>
              </ul>
              <a href="#." class="btn btn-orange">PURCHACE</a> </div>
          </div>
        </div>
      </div>
      -->
    </section>
    
    <!-- Flow Work  -->
    <section class="flow-work padding-top-80 padding-bottom-80">
      <div class="container"> 
        
        <!-- Tittle -->
        <div class="heading-block text-center margin-bottom-80">
          <h2>Notre Work Flow </h2>
          <span class="intro-style">Pour la réalisation de vos plateformes notre manière de procéder est la suivante: </span> </div>
        <div class="text-center"> <img src="images/work-folow-img.jpg" alt=" "> </div>
        <ul class="row padding-left-50 padding-right-50">
          <li class="col-sm-3">
            <div class="icon"> <img src="images/flow-icon-1.png" alt="" > </div>
            <h6>Analysons</h6>
            <p>Avec un travail basé sur l'échange nous récuperons les informations sur vos besoins et les traitons en interne.</p>
          </li>
          <li class="col-sm-3">
            <div class="icon"> <img src="images/flow-icon-2.png" alt="" > </div>
            <h6>Designons</h6>
            <p>Basé sur le Design Thinking,du Human Centered Design et du UX Design nous élaborons la manière dont le produit sera présenté aux utilisateurs finaux.</p>
          </li>
          <li class="col-sm-3">
            <div class="icon"> <img src="images/flow-icon-3.png" alt="" > </div>
            <h6>Developpons</h6>
            <p>Une fois après validation nous passons à la confection proprement dit du produit toujors en récuperant les retour du commanditaire et des utilisateurs finaux.</p>
          </li>
          <li class="col-sm-3">
            <div class="icon"> <img src="images/flow-icon-4.png" alt="" > </div>
            <h6>Deployons</h6>
            <p>Une fois les tests sur les échantillons d'utilisateurs terminés et leurs feedbacks intégrés la plateforme est mise à votre disposition.</p>
          </li>
        </ul>
      </div>
    </section>
    
    <!-- Our Clients  -->
    <section class="clients padding-bottom-100 padding-top-100">
      <div class="container"> 
        
        <!-- Tittle -->
        <div class="heading-block white-text text-center margin-bottom-80">
          <h2>Clients et partenaires:</h2>
          <span class="intro-style"> Cette liste de quelques uns d'entre eux valide notre politique interne fondée sur la qualité du travail concernant la relation que nous avons ou que nous avons eut avec ces structures.</span> </div>
        
        <!-- Clients Images -->
        <ul class="col-5 text-center" >
          <li> <img class="img-responsive" src="images/client-img-1.jpg" alt=""></li>
          <li> <img class="img-responsive" src="images/client-img-2.jpg" alt=""></li>
          <li> <img class="img-responsive" src="images/client-img-3.jpg" alt=""></li>
          <li> <img class="img-responsive" src="images/client-img-4.jpg" alt=""></li>
          <li> <img class="img-responsive" src="images/client-img-5.jpg" alt=""></li>
        </ul>
      </div>
    </section>
    
  
    
    <!-- Latest News -->
    <section class="latest-news padding-top-100 padding-bottom-100" id="blog">
      <div class="container"> 
        
        <!-- Tittle -->
        <div class="heading-block text-center margin-bottom-80">
          <h2> Nos dernières publications</h2>
          <span class="intro-style">Vous recherchez des conseils, éclaircissements ou mise au point sur certains aspects de notre travail ou de ceux des structures oeuvrant dans le même domaine, c'est ici que vous serez éclairé: </span> </div>
        
        <!-- News -->
        <div class="row"> 
          <!-- News 1 -->
          <?php
            while ($donnees = $reponse->fetch()){
           ?>
          <div class="col-md-6"> <a href="#."> <img class="img-responsive" src="DevCorpDashBoard/uploads/<?php echo $donnees['image']; ?>" alt=""> </a>
            <div class="news-detail">
              <div class="row">
                <div class="col-md-3 text-center">
                  <div class="avatar"> <img class="img-circle" src="images/avatar-1.png" alt=""> </div>
                  <p><?php echo $donnees['created_at']; ?></p>
                  <p><i class="fa fa-comment"></i>00</p>
                </div>
                <div class="col-md-9"> <a href="blog-single.php?id=<?php echo $donnees['id']?>" target="_blank"><?php echo $donnees['title']; ?></a>
                  <p><?php echo substr($donnees['content'],0,600);?></p>
                </div>
              </div>
            </div>
          </div>
            <?php }?>
                    

         
      </div>
      <div class="text-center margin-top-50"> <a href="blog.php?page=1" class="btn btn-orange">Voir Plus</a> </div>
    </section>
  </div>
  <!-- End Content --> 
  
  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">
        
        
        <!-- Folow Us -->
        <div class="col-md-12 padding-top-50">
          <div class="news-letter">
            <h6>Suivez-nous sur:</h6>
            <ul class="social_icons pull-left margin-left-50 margin-top-10">
              <li><a href="https://www.facebook.com/Digital.DevCorp" target="_blank"><i class="fa fa-facebook"></i></a></li>
              <li><a href="https://twitter.com/DevC4ASM" target="_blank"><i class="fa fa-twitter"></i></a></li>
              <li><a href="https://plus.google.com/u/0/+DevCorpFourASM" target="_blank"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="https://www.linkedin.com/in/devcorp/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
              
            </ul>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Footer Info -->
    <div class="footer-info">
      <div class="container">
        <div class="row"> 
          
          <!-- About -->
          <div class="col-md-4" id="contact"> <img class="margin-bottom-30" src="images/logo-footer.png" alt="" >
            <p>DEV CORP est une compagnie qui évolue dans le domaine des TICs en tant que fournisseur de services et de solutions informatiques. L'équipe a été constituée sur les bases de l'esprit groupe, la compétence, l’exigence en terme de qualité et la volonté d’entreprendre .En somme une équipe jeune, dynamique, qualifié, pleine d’inspiration et de créativité.</p>
            <ul class="personal-info">
              <li><i class="fa fa-map-marker"></i> 9, cité Asecna Batterie Yoff Aéroport</li>
              <li><i class="fa fa-envelope"></i> contact@devcorptechnologies.com</li>
			  <li><i class="fa fa-envelope"></i> ahmed.fall@devcorptechnologies.com</li>
			  <li><i class="fa fa-envelope"></i> ahmadou.fall@devcorptechnologies.com</li>
              <li><i class="fa fa-phone"></i> +221 76 660 30 10 </li>
			  <li><i class="fa fa-phone"></i> +221 77 306 77 19 </li>
            </ul>
          </div>         
          <!-- Service provided -->
           <div class="col-md-4">
            <h6>Envoyez nous un message</h6>
            <div class="quote">
              <form method="POST" action="message.php">
                <input class="form-control" name="nom" type="text" placeholder="Name">
                <input class="form-control" name="mail" type="email" placeholder="Email">
                <textarea class="form-control" name="message" placeholder="Messages"></textarea>
                <button type="submit" class="btn btn-orange">SEND NOW</button>
              </form>
            </div>
          </div>          
          <!-- Quote -->
          <div class="col-md-4">
            <h6>Facebook</h6>            
              <div class="fb-page" data-href="https://www.facebook.com/Digital.DevCorp" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Digital.DevCorp" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Digital.DevCorp">DEV CORP</a></blockquote></div>            
          </div>
        </div>
      </div>
    </div>   
    <!-- Rights -->
    <div class="rights">
      <div class="container">
        <p>Copyright © 2016 DEV CORP. All Rights Reserved to DEV CORP TECHNOLOGIES</p>
      </div>
    </div>
  </footer>
</div>
<!-- End Page Wrapper --> 
<!-- JavaScripts --> 
<script src="js/vendors/jquery/jquery.min.js"></script> 
<script src="js/vendors/wow.min.js"></script> 
<script src="js/vendors/bootstrap.min.js"></script> 
<script src="js/vendors/own-menu.js"></script> 
<script src="js/vendors/flexslider/jquery.flexslider-min.js"></script> 
<script src="js/vendors/jquery.countTo.js"></script> 
<script src="js/vendors/jquery.isotope.min.js"></script> 
<script src="js/vendors/jquery.bxslider.min.js"></script> 
<script src="js/vendors/owl.carousel.min.js"></script> 
<script src="js/vendors/jquery.sticky.js"></script> 
<!-- SLIDER REVOLUTION 4.x SCRIPTS  --> 
<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script> 
<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script> 
<script src="js/main.js"></script> 

</body>
</html>