<?php
require_once('verification.php');
require_once("connect.php");
$id = $_SESSION['id'];
if(!empty($_POST['titre']) && !empty(isset( $_POST['cat'])) && !empty(isset( $_POST['texte'])) )
{


$titre = htmlspecialchars($_POST['titre']);

$type = htmlspecialchars( $_POST['cat']);

$t = "";
if($type == 1){
    $t = "Web";
}
elseif ($type == 2) {
    $t = "Marketing Digital";
}elseif ($type == 3) {
    $t = "Infographie";
}elseif ($type == 4) {
    $t = "Blogging";
}elseif ($type == 5) {
    $t = "Social Media";
}
$text = $_POST['texte'];
// photo
  $target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["photo"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image

 
   
if(isset($_POST["submit"])) {    
    $check = getimagesize("uploads/".$_FILES["photo"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
      $error=4;
        $uploadOk = 0;
    }
}
// Check if file already exists
 if (file_exists($target_file)) {
      $error=5;
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["photo"]["size"] > 500000) {
        $error=6;
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
$error=7;

    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
list($width,$height,$type,$attr) = getimagesize($_FILES["photo"]["tmp_name"]);
 echo $width;
      if($width != 830 && $height != 426)
   {
       echo 'fichier trop large';
    $error= 3;
      $uploadOk=0;
   }  
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
    header("Location: ajouter.php?id=$id&error=$error");
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["photo"]["name"]). " has been uploaded.";
        $photo = $_FILES["photo"]["name"];
        $req = $bdd->prepare('INSERT INTO article (title,image,content,categorie)VALUES(?,?,?,?)');
$req->execute(array($titre,$photo, $text,$t));
    header("Location: ajouter.php?id=$id&error=0");
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}



}else if(empty($_POST['titre']) || empty(isset( $_POST['type'])) || empty(isset( $_POST['cat'])) || empty(isset( $_POST['texte'])) || empty(isset( $_POST['photo'])))
{
   header("Location: ajouter.php?id=$id&error=10"); 

}
?>