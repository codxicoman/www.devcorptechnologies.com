 
<?php
require_once('verification.php');
require_once("connect.php");
$id = $_SESSION['id'];

if(!empty($_POST['nom']) && !empty($_POST['prenom']) && !empty($_POST['mail'])  && !empty(isset( $_POST['password1'])) && !empty(isset( $_POST['password2'])) )
{
$nom=isset($_POST['nom']) ? $_POST['nom'] : '';
$prenom=isset($_POST['prenom']) ? $_POST['prenom'] : '';
$mail =isset($_POST['mail']) ? $_POST['mail'] : '';
$password1 =sha1(isset($_POST['password1']) ? $_POST['password1'] : '');
$password2 =sha1(isset($_POST['password2']) ? $_POST['password2'] : '');
// photo
$target_dir = "uploads/";
$target_file = $target_dir . basename($_FILES["photo"]["name"]);
$uploadOk = 1;
$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
// Check if image file is a actual image or fake image

if(isset($_POST["submit"])) {
    $check = getimagesize($_FILES["photo"]["tmp_name"]);
    if($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
    } else {
        echo "File is not an image.";
                header("Location: ajout_editeur.php?id=$id&error=4");

        $uploadOk = 0;
    }
}
// Check if file already exists
if (file_exists($target_file)) {
        header("Location: ajout_editeur.php?id=$id&error=5");
    echo "Sorry, file already exists.";
    $uploadOk = 0;
}
// Check file size
if ($_FILES["photo"]["size"] > 500000) {
         header("Location: ajout_editeur.php?id=$id&error=6");
    echo "Sorry, your file is too large.";
    $uploadOk = 0;
}
// Allow certain file formats
if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
&& $imageFileType != "gif" ) {
         header("Location: ajout_editeur.php?id=$id&error=7");

    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
    $uploadOk = 0;
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
    echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
} else {
    if (move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file)) {
        echo "The file ". basename( $_FILES["photo"]["name"]). " has been uploaded.";
        $photo = $_FILES["photo"]["name"];
    } else {
        echo "Sorry, there was an error uploading your file.";
    }
}


// requete
if($password1 == $password2)
{
    $req = $bdd->prepare('INSERT INTO editeurs (firstName,lastName,email,password,image)VALUES(?,?,?,?,?)');

$req->execute(array($prenom,$nom, $mail,$password2,$photo));
header('Location: ajout_editeur.php?success=0');
}
else{
     header("Location: ajout_editeur.php?id=$id&error=9"); 
    echo "Mot de passe non valide";
}
}
else{
    header("Location: ajout_editeur.php?id=$id&error=10"); 
}
?>