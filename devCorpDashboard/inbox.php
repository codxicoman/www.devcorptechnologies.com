<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Boite De Reception</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->

    <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="all" />
    <link rel="stylesheet" href="css/style1.css">
    <!--Let browser know website is optimized for mobile-->

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>
    <nav>
        <div class="nav-wrapper">
            <a href="#!" class="brand-logo">Rene Andre</a>
            <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                <li>
                    <a href="#"> <i class="material-icons left">search</i></a>
                </li>
                <li>
                    <a href="#"> <i class="material-icons left">message</i></a>
                </li>
                <li>
                    <a class='dropdown-button drop' href='#' data-activates='dropdown1'>
                        <img src="img/yuna.jpg" alt="" class="circle cercle responsive-img">
                    </a>
                </li>
            </ul>
            <ul id='dropdown1' class='dropdown-content' class="btn">
                <li><a class="btn-floating btn-large red"><i class="material-icons">perm_identity</i></a></li>
                <li><a class="btn-floating  btn-large yellow darken-1"><i class="material-icons">format_quote</i></a></li>
                <li><a class="btn-floating  btn-large green"><i class="material-icons">publish</i></a></li>
                <li><a class="btn-floating  btn-large blue"><i class="material-icons">settings</i></a></li>
            </ul>
            <ul class="side-nav" id="mobile-demo">
                <li>
                    <div class="userView">
                        <div class="background">
                            <img src="img/office.jpg">
                        </div>
                        <a href="#!user"><img class="circle" src="img/yuna.jpg"></a>
                        <a href="#!name"><span class="white-text name">René André Junior Coly</span></a>
                        <a href="#!email"><span class="white-text email">jdandturk@gmail.com</span></a>
                    </div>
                </li>
                <ul class=" listItem">
                <li><a class="item" href="index.html"><i class="small material-icons ">dashboard</i><p>Tableau de Bord</p></a></li>
                <li><a class="item" href="ajouter.html"><i class="small material-icons ">add</i><p>Ajouter</p></a></li>
                <li><a class="item" href="afficher.html"><i class="small material-icons">view_list</i><p>Afficher</p></a></li>
                <li class="collap">
                    <ul class="collapsible mess" data-collapsible="accordion">
                        <li>
                            <div class="collapsible-header collapseHeader " class=" "><i class="small material-icons">email</i>
                                <p>message</p>
                            </div>
                            <div class="collapsible-body collapseBody white">
                                <ul>
                                    <li><a class="temp white z-depth-5" href="inbox.html"><i class="small material-icons ">inbox</i><p>Boite de Reception</p></a></li>
                                    <li><a class="temp white z-depth-5" class="white" href="#modal1"><i class="small material-icons ">send</i><p>Envoyes</p></a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>
                <li><a class="item" href="#!"><i class="small material-icons">shopping_cart</i><p>Commande</p></a></li>
            </ul>
            </ul>

        </div>
    </nav>
    <div class="row content">
        <div class="hide-on-small-only panel col s12 m4 l3 white darken-3 z-depth-2">
            <!-- Grey navigation panel -->
            <ul class=" listItem">
                <li><a class="item" href="index.html"><i class="small material-icons ">dashboard</i><p>Tableau de Bord</p></a></li>
                <li><a class="item" href="ajouter.html"><i class="small material-icons ">add</i><p>Ajouter</p></a></li>
                <li><a class="item" href="afficher.html"><i class="small material-icons">view_list</i><p>Afficher</p></a></li>
                <li class="collap">
                    <ul class="collapsible mess" data-collapsible="accordion">
                        <li>
                            <div class="collapsible-header collapseHeader " class=" "><i class="small material-icons">email</i>
                                <p>message</p>
                            </div>
                            <div class="collapsible-body collapseBody white">
                                <ul>
                                    <li><a class="temp white z-depth-5" href="inbox.html"><i class="small material-icons ">inbox</i><p>Boite de Reception</p></a></li>
                                    <li><a class="temp white z-depth-5" class="white" href="#modal1"><i class="small material-icons ">send</i><p>Envoyes</p></a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>
                <li><a class="item" href="#!"><i class="small material-icons">shopping_cart</i><p>Commande</p></a></li>
            </ul>
        </div>
        <div class="col s12 m8 l9">
            <div class="container">
                <div class="section">
                    <div class="row">
                        <div class="cols12">
                            <nav class="red">
                                <div class="nav-wrapper">
                                    <div class="left col s12 m5 l5">
                                        <ul>
                                            <li><a href="#!" class="email-menu"><i class="mdi-navigation-menu"></i></a>
                                            </li>
                                            <li><a href="#!" class="email-type">Boite de Reception</a>
                                            </li>
                                            <li class="right"><a href="#!"><i class="mdi-action-search"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col s12 m7 l7 hide-on-med-and-down">
                                        <ul class="right">
                                            <li><a href="#!"><i class="material-icons">archive</i></a>
                                            </li>
                                            <li><a href="#!"><i class="material-icons">delete</i></a>
                                            </li>
                                            <li><a href="#!"><i class="material-icons">email</i></a>
                                            </li>
                                            <li><a href="#!"><i class="material-icons">more_vert</i></a>
                                            </li>
                                        </ul>
                                    </div>
                            </nav>
                            </div>
                            <div class="row">
                                <div class="col s12">
                                    <div id="email-list" class="col s12 m5 l5 card-panel z-depth-1">
                                        <ul class="collection">
                                            <li class="collection-item avatar">
                                                <img src="img/yuna.jpg" alt="" class="circle">
                                                <span class="mailTitle">René Junior Coly</span>
                                                <p class="truncate grey-text ultra-small">Lorem ipsum dolor set.</p>
                                                <a href="#!" class="secondary-content time">
                                                    <span class="blue-text ultra-small">2:05 am</span>
                                                </a>
                                            </li>
                                            <li class="collection-item avatar">
                                                <span class="circle red lighten-1">A</span>
                                                <span class="mailIitle">Amazon</span>
                                                <p class="truncate grey-text ultra-small">Lorem ipsum dolor set.</p>
                                                <a href="#!" class="secondary-content time">
                                                    <span class="blue-text ultra-small">2:05 am</span>
                                                </a>
                                            </li>
                                            <li class="collection-item avatar">
                                                <span class="circle blue lighten-1">B</span>
                                                <span class="mailTitle">Bitcoin</span>
                                                <p class="truncate grey-text ultra-small">Lorem ipsum dolor set.</p>
                                                <a href="#!" class="secondary-content time">
                                                    <span class="blue-text ultra-small">2:05 am</span>
                                                </a>
                                            </li>
                                            <li class="collection-item avatar">
                                                <span class="circle green lighten-1">M</span>
                                                <span class="title">Moussa</span>
                                                <p class="truncate grey-text ultra-small">Lorem ipsum dolor set.</p>
                                                <a href="#!" class="secondary-content time">
                                                    <span class="blue-text ultra-small">2:05 am</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div id="mailDetails" class="col s12 m7 l7 card-panel">
                                        <p class="email-subject truncate">Message Details <span class="email-tag grey lighten-3">inbox</span>
                                            
                                        </p>
                                        <hr class="grey-text text-lighten-2">
                                        <div class="email-content-wrap">
                                            <div class="row">
                                                <div class="col s10 m10 l10">
                                                    <ul class="collection">
                                                        <li class="collection-item avatar">
                                                            <img src="img/yuna.jpg" alt="" class="circle">
                                                            <span class="mailTitle">René Junior Coly</span>
                                                            <p class=" grey-text ultra-small">To Doux Pinw</p>
                                                            <p class=" grey-text ultra-small">Yesterday</p>

                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col s2 m2 l2 email-actions">
                                                    <a href="#!"><span><i class="mdi-content-reply"></i></span></a>
                                                    <a href="#!"><span><i class="mdi-navigation-more-vert"></i></span></a>
                                                </div>
                                            </div>
                                            <div class="mailContent">
                                                <p>Bonjour Doux Pinw</p>
                                                <p>Veuillez diminuer de peu le niveau de domeramé que tu as ainsi que tous les
                                                    gens de ta chambre à l'exeption du venerable Mouhamed Dieye.
                                                </p>
                                                <p>Veuillez agreer mes salutations Meilleures
                                                    <br>Mike Parker</p>
                                            </div>
                                        </div>
                                        <div class="email-reply">
                                            <div class="row">
                                                <div class="col s4 m4 l4 center-align">
                                                    <a href="!#"><i class="material-icons">reply</i></a>
                                                    <p class="ultra-small">Reply</p>
                                                </div>
                                                <div class="col s4 m4 l4 center-align">
                                                    <a href="!#"><i class="material-icons">reply_all</i></a>
                                                    <p class="ultra-small">Reply all</p>
                                                </div>
                                                <div class="col s4 m4 l4 center-align">
                                                    <a href="!#"><i class="material-icons">forward</i></a>
                                                    <p class="ultra-small">Forward</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="fixed-action-btn" style="bottom: 50px; right: 19px;">
                    <a class="btn-floating btn-large red modal-trigger" href="#modal1"> <i class="material-icons editer">border_color</i></a>
                </div>
                <div id="modal1" class="modal" style="z-index: 1003; display: none; opacity: 0; transform: scaleX(0.7); top: 208.696px;">
                <div class="modal-content">
                    <nav class="red">
                        <div class="nav-wrapper">
                            <div class="left col s12 m5 l5">
                                <ul>
                                    <li><a href="#!" class="email-menu"><i class="modal-action modal-close material-icons">keyboard_backspace</i></a>
                                    </li>
                                    <li><a href="#!" class="email-type">Rediger</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col s12 m7 l7 hide-on-med-and-down">
                                <ul class="right">
                                    <li><a href="#!"><i class="material-icons">attach_file</i></a>
                                    </li>
                                    <li><a href="#!"><i class="modal-action modal-close  material-icons ">send</i></a>
                                    </li>
                                    <li><a href="#!"><i class="material-icons">more_vert</i></a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                    </nav>
                </div>
                <div class="model-email-content">
                    <div class="row">
                        <form class="col s12">
                            <!--<div class="row">
                                    <div class="input-field col s12">
                                        <input id="from_email" type="email" class="validate">
                                        <label for="from_email">From</label>
                                    </div>
                                    </div> -->
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="to_email" type="email" class="validate">
                                    <label for="to_email" class="">To</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <input id="subject" type="text" class="validate">
                                    <label for="subject" class="">Subject</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="input-field col s12">
                                    <textarea id="compose" class="materialize-textarea" length="500"></textarea>
                                    <label for="compose">Compose email</label>
                                    <span class="character-counter" style="float: right; font-size: 12px; height: 1px;"></span>
                </div>
                </div>
                </form>
                </div>
</div>
</div>
</div>
</div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
</body>

</html>