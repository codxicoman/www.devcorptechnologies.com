<?php

require_once('verification.php');
require_once('connect.php');
$supp = $_GET['id']; 


$reponse = $bdd->query("SELECT * FROM article WHERE id=$supp ");
$id = $_SESSION ['id'] ;
 $req = $bdd->query("SELECT * FROM editeurs WHERE id=$id");
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen" />
    <link rel="stylesheet" href="css/style.css">
    <!--Let browser know website is optimized for mobile-->

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
    <div class="row ">
            <div class="col l2">        
            <?php             
		while ($donne = $req->fetch()){
		?>      
             <ul id="slide-out" class="side-nav fixed">
                <li>
                    <div class="userView">
                        <div class="background">
                            <img src="uploads/<?php echo $donne['image']; ?>">
                        </div>
                        <a href="#!user"><img class="circle" src="uploads/<?php echo $donne['image']; ?>"></a>
                        <a href="#!name"><span class="inherit-text name"><?php echo $donne['firstName']?> <?php echo $donne['lastName']?></span></a>
                        <a href="#!email"><span class="inherit-text email"><?php echo $donne['email']?></span></a>
                    </div>
                </li >
                <li><a class="item" href="index.html"><i class="small material-icons ">dashboard</i><p>Tableau de Bord</p></a></li>

                <li><a class="item" href="ajouter.php"><i class="small material-icons ">add</i><p>Ajouter</p></a></li>
                <li class="edit"><a class="item" href="editer.php"><i class="small material-icons ">edit</i><p>Editer</p></a></li>
                <li><a class="item" href="afficher.php"><i class="small material-icons">view_list</i><p>Afficher</p></a></li>
                <li><a class="item" href="logout.php"><i class="small material-icons">lock</i><p>Se Deconnecter</p></a></li>
            </ul>
            <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
   <?php
                     
		}

		?>

   </div> 
   <div class="col l9 push-l1">
       <nav>
           <img src="img/logo.png" class="brand-logo left" alt="">
        
         <a href="#!" class="right"> Logout</a>
       </nav>
       <div class="redaContain">
                <div class="col s12  z-depth-4 card-panel">
                       <form method="POST" action="editer_post.php?id=<?php echo $supp?>" enctype="multipart/form-data" class="login-form ">
                     <?php
                     
		while ($donnees = $reponse->fetch()){

		?>
      
                        <div class="row center">
                            <div class="input-field col s12 center-align">
                                <input id="titre" name="titre" type="text" class="validate champ">
                                    
                                <label for="titre"><?php echo $donnees['title']?></label>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="input-field col s12 center-align">
                                <input id="titre" name="cat" type="text" class="validate">
                                    
                                <label for="titre"><?php echo $donnees['categorie']?></label>
                            </div>
                        </div>
                        <div class="row  center">
                            <div class="input-field col s12 center-align">
                                <div class="col s12">
                                <?php if ($donnees['image'] == NULL){?>
                                 <p class="center"> image Non disponible</p> 
                                 <?php }
                                 else {
                                ?>
                                   <img class="responsive-img" src="uploads/<?php echo $donnees['image']; ?>"/>     
                        <?php }?>
                                 </div>   
                            </div>
                        </div>
                       <div class="file-field input-field">
                      <div class="btn">
                             <span>Charger Photo</span>
                            <input type="file" name="photo">
                         </div>
                    <div class="file-path-wrapper">
                     <input class="file-path validate" type="text">
                    </div>
                     </div>
                        <div class="row">
                            <div class="input-field col s12">
                                <textarea id="texte" name="texte" class="materialize-textarea"><?php echo $donnees['content']?></textarea>
                                <label for="texte">Article</label>
                            </div>
                        </div>
                        <button class="btn waves-effect waves-light" type="submit" name="action">Enregistrer Modification
                            <i class="material-icons right">send</i>
                        </button>
                        <?php } ?>
                        </form>
                </div>
        </div>
   </div>
    </div>
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
        
         <script type="text/javascript" src="js/script.js"></script>
         <script src="ckeditor/ckeditor.js"></script>
      <script >
    CKEDITOR.replace( 'texte' );
    </script>
     <?php 
           
              if(!empty(isset($_GET['error']))|| isset($_GET['success']))
{
 $error = $_GET['error']; 
 if(!empty(isset($_GET['success'])))
 $success = $_GET['success']; 
              if($error == 3){?>
               <script>   
                  Materialize.toast('<strong>Enregistrement non reussi :</strong> la photo doit etre de 830 de longueur et 426 de large </div>', 5000,'red');
               </script> 
             <?php }
              else if($error == 4){?>
              <script>
                 Materialize.toast(' <strong>Enregistrement non reussi :</strong> Vous n\'avez pas enregistrer de photo</div>', 5000,'red');
              </script> 
              <?php
              }else if($error == 5){?>
             <script>
                Materialize.toast('<strong>Enregistrement non reussi :</strong> le fichier existe dejà</div>', 5000,'red');
             </script> 
              <?php
              }else if($error == 6){?>
             <script>
                Materialize.toast('<strong>Enregistrement non reussi :</strong> Vous n\'avez pas enregistrer de photo</div>', 5000,'red');
             </script>       
              <?php
              }else if($error == 7){?>
              <script>
                 Materialize.toast('<strong>Enregistrement non reussi :</strong>Seul JPG,JPEG,PNG & GIF sont autorisés</div>', 5000,'red');
              </script>           
              <?php
              }else if($error == 10){?>
              <script>
                 Materialize.toast(' <strong>Enregistrement non reussi :</strong>Veuillez remplir tous les champs</div>', 5000,'red');
              </script>
              <?php
              }else if($success == 0){?>
              <script>
                 Materialize.toast(' <strong>Enregistrement reussi ', 5000,'green');
              </script>
             <?php }
              ?>
            <?php
            }?>
</body>

</html>