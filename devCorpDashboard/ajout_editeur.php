<?php
require_once('verification.php');
require_once('verification.php');
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Admin |devcorptechnologies</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen" />
    <link rel="stylesheet" href="css/style.css">
    <!--Let browser know website is optimized for mobile-->

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>

    <div class="row content ">

        <!-- <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
           <ul class="right hide-on-med-and-down">
                <li><a href="#!">First Sidebar Link</a></li>
                <li><a href="#!">Second Sidebar Link</a></li>
            </ul>-->
        <div class="col l2">
            <ul id="slide-out" class="side-nav fixed">
                <li>
                    <div class="userView">
                        <div class="background">
                            <img src="img/office.jpg">
                        </div>
                     <a href="#!user"><img class="circle" src="img/avatar-1.png"></a>
                        <a href="#!name"><span class="white-text name">Bobo</span></a>
                        <a href="#!email"><span class="white-text email">contact@devcorptechnologies.com</span></a>
                    </div>
                </li>
                <li><a class="item" href="index.html"><i class="small material-icons ">dashboard</i><p>Tableau de Bord</p></a></li>

                <li><a class="item" href="ajout_editeur.php"><i class="small material-icons ">add</i><p>Ajouter</p></a></li>
                <li><a class="item" href="inbox.php"><i class="small material-icons ">edit</i><p>Boite de reception</p></a></li>
                <li><a class="item" href="afficher_editeur.php"><i class="small material-icons">view_list</i><p>Afficher</p></a></li>
                <li><a class="item" href="deconnecter.php"><i class="small material-icons">lock</i><p>Se Deconnecter</p></a></li>
            </ul>
            <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>

        </div>
        <div class="col l9 push-l1">
            <nav>
                <img src="img/logo.png" class="brand-logo left" alt="">

                <a href="logout.php" class="right"> Logout</a>
            </nav>
            <div class="redaContain">
                <div class="row  center z-depth-4 card-panel ">
    <form class="col s12" method="POST" action="inscription_post.php" enctype="multipart/form-data">
      <div class="row">
        <div class="input-field col s6">
          <input placeholder="Placeholder" name="prenom" id="first_name" type="text"  class="validate">
          <label for="first_name">First Name</label>
        </div>
        <div class="input-field col s6">
          <input placeholder="Placeholder" name="nom" id="first_name" type="text"  class="validate">
          <label for="first_name">Last Name</label>
        </div>
      </div>
       <div class="row">
        <div class="input-field col s12">
          <input id="password" type="password" name="password1" style="width:50%;" class="validate">
          <label for="password">Password</label>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
          <input id="password" type="password" name="password2" style="width:50%;" class="validate">
          <label for="password">Retaper Password</label>
        </div>
      </div>
      <div class="file-field input-field">
            <div class="btn">
                <span>Charger Photo</span>
                <input type="file"  name="photo">
            </div>
            <div class="file-path-wrapper">
                <input class="file-path validate" type="text">
            </div>
    </div>
      <div class="row">
        <div class="input-field col s12">
          <input id="email" type="email" name="mail" style="width:50%;" class="validate">
          <label for="email">Email</label>
        </div>
      </div>
      <button class="btn waves-effect waves-light" type="submit" name="action">Ajouter
    <i class="material-icons right">send</i>
  </button>
    </form>
  </div>
        
            </div>
        </div>
    </div>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>

    <script type="text/javascript" src="js/script.js"></script>
     <?php 
           
              if(!empty(isset($_GET['error']))|| isset($_GET['success']))
{
 $error = $_GET['error']; 
 if(!empty(isset($_GET['success'])))
 $success = $_GET['success']; 
              if($error == 3){?>
               <script>   
                  Materialize.toast('<strong>Enregistrement non reussi :</strong> la photo doit etre de 830 de longueur et 426 de large </div>', 5000,'red');
               </script> 
             <?php }
              else if($error == 4){?>
              <script>
                 Materialize.toast(' <strong>Enregistrement non reussi :</strong> Vous n\'avez pas enregistrer de photo</div>', 5000,'red');
              </script> 
              <?php
              }else if($error == 5){?>
             <script>
                Materialize.toast('<strong>Enregistrement non reussi :</strong> le fichier existe dejà</div>', 5000,'red');
             </script> 
              <?php
              }else if($error == 6){?>
             <script>
                Materialize.toast('<strong>Enregistrement non reussi :</strong> Vous n\'avez pas enregistrer de photo</div>', 5000,'red');
             </script>       
              <?php
              }else if($error == 7){?>
              <script>
                 Materialize.toast('<strong>Enregistrement non reussi :</strong>Seul JPG,JPEG,PNG & GIF sont autorisés</div>', 5000,'red');
              </script>           
              <?php
              }else if($error == 9){?>
              <script>
                 Materialize.toast('<strong>Enregistrement non reussi :</strong>Les mots de passe ne correspondent pas</div>', 5000,'red');
              </script>           
              <?php
              }else if($error == 10){?>
              <script>
                 Materialize.toast(' <strong>Enregistrement non reussi :</strong>Veuillez remplir tous les champs</div>', 5000,'red');
              </script>
              <?php
              }else if($success == 0){?>
              <script>
                 Materialize.toast(' <strong>Enregistrement reussi ', 5000,'green');
              </script>
             <?php }
              ?>
            <?php
            }?>
</body>

</html>