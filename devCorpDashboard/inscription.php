<?php
require_once('verification.php');
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inscription</title>
    <meta name="description" content="" />
    <meta name="author" content="templatemo">
<link rel="shortcut icon" href="foj.png"/>	
    <link href="login/login.css" rel="stylesheet">

  </head>
  <body>
<hgroup>
  <h1>Inscription</h1>
</hgroup>
<form action="inscription_post.php" method="post">
  <div class="group">
    <input type="text" name="nom"><span class="highlight"></span><span class="bar"></span>
    <label>Nom</label>
  </div>
  <div class="group">
    <input type="text" name="prenom"><span class="highlight"></span><span class="bar"></span>
    <label>Prenom</label>
  </div>
  <div class="group">
    <input type="text" name="login"><span class="highlight"></span><span class="bar"></span>
    <label>Login</label>
  </div>
  <div class="group">
    <input type="password" name="password1"><span class="highlight"></span><span class="bar"></span>
    <label>Ancien Mot de Passe</label>
  </div>
   <div class="group">
    <input type="password" name="password2"><span class="highlight"></span><span class="bar"></span>
    <label> Nouveau Mot de Passe</label>
  </div>
  <div class="group">
    <input type="password" name="password3"><span class="highlight"></span><span class="bar"></span>
    <label> Retaper Mot de Passe</label>
  </div>
  <button type="submit" class="button buttonBlue">Valider
    <div class="ripples buttonRipples"><span class="ripplesCircle"></span></div>
  </button>
</form>
<<footer><a href="http://www.polymer-project.org/" target="_blank"><img src="login/log.jpg"></a>
  <p>You Gotta Love <a href="http://www.fojeula.org/" target="_blank">Fojeula</a></p>
</footer>
<script src="login/js/jquery.js"></script>
<script src="login/login.js"></script>
  </body>
  </html>