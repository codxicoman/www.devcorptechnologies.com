<!DOCTYPE html>
<html class="contain" lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen" />
    <link rel="stylesheet" href="css/style.css">
    <!--Let browser know website is optimized for mobile-->
    <script src="http://d3js.org/d3.v3.min.js" language="JavaScript"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body class="cyan login">
    <div id="login-page" class="row loginContent">
        <div class="col s12 z-depth-4 card-panel">
            <form class="login-form" action="login.php" enctype="multipart/form-data" method="POST" >
                <div class="row">
                    <div class="input-field col s12 center">
                        <img src="img/logo.png" alt="" class="valign profile-image-login">
                        <p class="center login-form-text">Se connecter</p>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s12">
                        <i class="material-icons prefix">account_circle</i>
                        <input id="icon_prefix" name="mail" type="email" class="validate">
                        <label for="icon_prefix">Email</label>
                    </div>
                    <div class="input-field col s12">
                        <i class="material-icons prefix">lock</i>
                        <input id="icon_telephone" name="password" type="password" class="validate">
                        <label for="icon_telephone">Mot de passe</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <button class="btn waves-effect waves-light" type="submit" name="action">Enregistrer
                            <i class="material-icons right">send</i>
                        </button>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s6 m6 l6">
                        <p class="margin medium-small"><a href="page-register.html">Register Now!</a></p>
                    </div>
                    <div class="input-field col s6 m6 l6">
                        <p class="margin right-align medium-small"><a href="page-forgot-password.html">Forgot password ?</a></p>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script src="js/liquidFillGauge.js" language="JavaScript"></script>
    <script type="text/javascript" src="js/script.js"></script>
</body>

</html>