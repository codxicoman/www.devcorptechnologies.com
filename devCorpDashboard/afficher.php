<?php 
require('verification.php');
require_once("connect.php");
$reponse = $bdd->query('SELECT *  FROM article ');
$id = $_SESSION ['id'] ;
 $req = $bdd->query("SELECT * FROM editeurs WHERE id=$id");
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen" />
    <link rel="stylesheet" href="css/style.css">
    <!--Let browser know website is optimized for mobile-->

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>

    <div class="row ">
        
    <!-- <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
           <ul class="right hide-on-med-and-down">
                <li><a href="#!">First Sidebar Link</a></li>
                <li><a href="#!">Second Sidebar Link</a></li>
            </ul>-->
            <div class="col l2">  
              <?php             
		while ($donne = $req->fetch()){
		?>      
             <ul id="slide-out" class="side-nav fixed">
                <li>
                    <div class="userView">
                        <div class="background">
                            <img src="uploads/<?php echo $donne['image']; ?>">
                        </div>
                        <a href="#!user"><img class="circle" src="uploads/<?php echo $donne['image']; ?>"></a>
                        <a href="#!name"><span class="inherit-text name"><?php echo $donne['firstName']?> <?php echo $donne['lastName']?></span></a>
                        <a href="#!email"><span class="inherit-text email"><?php echo $donne['email']?></span></a>
                    </div>
                </li >
                <li><a class="item" href="index.html"><i class="small material-icons ">dashboard</i><p>Tableau de Bord</p></a></li>

                <li><a class="item" href="ajouter.php"><i class="small material-icons ">add</i><p>Ajouter</p></a></li>
                <li class="edit"><a class="item" href="editer.php"><i class="small material-icons ">edit</i><p>Editer</p></a></li>
                <li><a class="item" href="afficher.php"><i class="small material-icons">view_list</i><p>Afficher</p></a></li>
                <li><a class="item" href="logout.php"><i class="small material-icons">lock</i><p>Se Deconnecter</p></a></li>
            </ul>
            <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
   <?php
                     
		}

		?>
   </div> 
   <div class="col l10">
       <nav >
           <img src="img/logo.png" class="brand-logo left" alt="">
        
         
       </nav>
        <div class="row search">
        <div class="searching  center">
            <div class="nav-wrapper ">
                <form>
                    <div class="input-field card searchContent z-depth-2">
                        <input id="search" placeholder="table chaise banc ect." class="inputSearch" type="search" required>
                        
                        <i class="material-icons close">close</i>
                    </div>
                    <div class="row  btnSearch">
                    <div class="input-field col s12">
                        <a href="index.html" type="submit" class="btn waves-effect waves-light col s12">rechercher</a>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
     <div class="row tableList">
        <h3 class="tableTitle">Liste des articles</h3>
        <table class="responsive-table centered">
        <thead>
          <tr>
              <th data-field="id">id</th>
              <th data-field="Titre">Titre Article</th>
              <th data-field="name">Categorie</th>
              <th data-field="price">Creé à</th>
              <th data-field="price">option</th>
              
              

          </tr>
        </thead>

        <tbody>
            <?php
            $i=1;
		while ($donnees = $reponse->fetch()){
         
		?>
          <tr>
            <td><?php echo $i?></td>
            <td><?php echo $donnees['title']?></td>
            <td><?php echo $donnees['categorie']?></td>
              <td><?php echo $donnees['created_at']?></td>
             <td> <a href="delete.php?id=<?php echo $donnees['id']?>"><i class="material-icons">delete</i></a>
              <a href="editer.php?id=<?php echo $donnees['id']?>"> <i class="material-icons">mode_edit</i></a></td>
          </tr>
        <?php 
            $i++;
        }?>
         <!-- <tr>
            <td>Alan</td>
            <td>Jellybean</td>
            <td>$3.76</td>
           
            <td> <a><i class="material-icons">delete</i></a> <a> <i class="material-icons">mode_edit</i></a></td>
            
          </tr>
          <tr>
            <td>Jonathan</td>
            <td>Lollipop</td>
            <td>$7.00</td>
            
            <td> <a><i class="material-icons">delete</i></a> <a> <i class="material-icons">mode_edit</i></a></td>

          </tr>-->
        </tbody>
      </table>
   
        </div>
   </div>
    </div>
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
        
         <script type="text/javascript" src="js/script.js"></script>
                    <?php 
           
              if(!empty(isset($_GET['success'])))
{
 $success = $_GET['success']; 
              if($success == 0){?>
               <script>   
                  Materialize.toast('<strong>Supression reussi  :</strong> l\'article a été supprimé avec success </div>', 5000,'green');
               </script>
               <?php
               }if($success == 1){?>
               <script>   
                  Materialize.toast('<strong>Modification reussi  :</strong> l\'article a été modifié avec success </div>', 5000,'green');
               </script>
               <?php
               }}?>
</body>

</html>