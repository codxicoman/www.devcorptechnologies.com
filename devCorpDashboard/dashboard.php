<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen" />
    <link rel="stylesheet" href="css/style.css">
    <!--Let browser know website is optimized for mobile-->

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>

    <div class="row ">
        
    <!-- <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
           <ul class="right hide-on-med-and-down">
                <li><a href="#!">First Sidebar Link</a></li>
                <li><a href="#!">Second Sidebar Link</a></li>
            </ul>-->
            <div class="col l2">        
            <ul id="slide-out" class="side-nav fixed">
                <li>
                    <div class="userView">
                        <div class="background">
                            <img src="img/office.jpg">
                        </div>
                        <a href="#!user"><img class="circle" src="img/yuna.jpg"></a>
                        <a href="#!name"><span class="white-text name">John Doe</span></a>
                        <a href="#!email"><span class="white-text email">jdandturk@gmail.com</span></a>
                    </div>
                </li>
                <li><a class="item" href="index.html"><i class="small material-icons ">dashboard</i><p>Tableau de Bord</p></a></li>

                <li><a class="item" href="ajouter.html"><i class="small material-icons ">add</i><p>Ajouter</p></a></li>
                <li><a class="item" href="ajouter.html"><i class="small material-icons ">edit</i><p>Editer</p></a></li>
                <li><a class="item" href="afficher.html"><i class="small material-icons">view_list</i><p>Afficher</p></a></li>
            </ul>
            <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>

   </div> 
       <nav class="col l10">

       </nav>
   </div>
    </div>
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
        
         <script type="text/javascript" src="js/script.js"></script>
</body>

</html>