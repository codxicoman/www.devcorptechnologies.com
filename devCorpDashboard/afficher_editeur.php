<?php 
require('verification.php');
require_once("connect.php");
$reponse = $bdd->query('SELECT *  FROM article ');
$id = $_SESSION ['id'] ;
 $req = $bdd->query("SELECT * FROM editeurs ");
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
   <title>Admin | devcorptechnologies</title>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen" />
    <link rel="stylesheet" href="css/style.css">
    <!--Let browser know website is optimized for mobile-->

    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>

<body>

    <div class="row ">
        
    <!-- <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
           <ul class="right hide-on-med-and-down">
                <li><a href="#!">First Sidebar Link</a></li>
                <li><a href="#!">Second Sidebar Link</a></li>
            </ul>-->
            <div class="col l2">  
             
             <ul id="slide-out" class="side-nav fixed">
                <li>
                    <div class="userView">
                        <div class="background">
                            <img src="img/office.jpg">
                        </div>
                        <a href="#!user"><img class="circle" src="img/avatar-1.png"></a>
                        <a href="#!name"><span class="white-text name">Bobo</span></a>
                        <a href="#!email"><span class="white-text email">contact@devcorptechnologies.com</span></a>
                    </div>
                </li>
                <li><a class="item" href="ajouter.php"><i class="small material-icons ">add</i><p>Ajouter</p></a></li>
                <li><a class="item" href="afficher.php"><i class="small material-icons">view_list</i><p>Afficher</p></a></li>
                <li><a class="item" href="deconnecter.php"><i class="small material-icons">lock</i><p>Se Deconnecter</p></a></li>
            </ul>
            <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
 
		
   </div> 
   <div class="col l10">
       <nav >
           <img src="img/logo.png" class="brand-logo left" alt="">
        
         
       </nav>
       
     <div class="row tableList">
        <h3 class="tableTitle">Liste des editeurs</h3>
        <table class="responsive-table centered">
        <thead>
          <tr>
              <th data-field="id">id</th>
              <th data-field="Titre">Nom</th>
              <th data-field="name">Prenom</th>
              <th data-field="price">Creé à</th>
              <th data-field="price">option</th>
              
              

          </tr>
        </thead>

        <tbody>
            <?php
            $i=1;
		while ($donne = $req->fetch()){
         
		?>
          <tr>
            <td><?php echo $i?></td>
            <td><?php echo $donne['firstName']?></td>
            <td><?php echo $donne['lastName']?></td>
              <td><?php echo $donne['email']?></td>
             <td> <a href="delete_editeur.php?id=<?php echo $donne['id']?>"><i class="material-icons">delete</i></a>
          </tr>
        <?php 
            $i++;
        }?>
         <!-- <tr>
            <td>Alan</td>
            <td>Jellybean</td>
            <td>$3.76</td>
           
            <td> <a><i class="material-icons">delete</i></a> <a> <i class="material-icons">mode_edit</i></a></td>
            
          </tr>
          <tr>
            <td>Jonathan</td>
            <td>Lollipop</td>
            <td>$7.00</td>
            
            <td> <a><i class="material-icons">delete</i></a> <a> <i class="material-icons">mode_edit</i></a></td>

          </tr>-->
        </tbody>
      </table>
   
        </div>
   </div>
    </div>
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="js/materialize.min.js"></script>
        
         <script type="text/javascript" src="js/script.js"></script>
                    <?php 
           
              if(!empty(isset($_GET['success'])))
{
 $success = $_GET['success']; 
              if($success == 0){?>
               <script>   
                  Materialize.toast('<strong>Supression reussi  :</strong> l\'article a été supprimé avec success </div>', 5000,'green');
               </script>
               <?php
               }}?>
</body>

</html>