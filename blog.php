<?php
require_once("devCorpDashboard/connect.php");
$reponse = $bdd->query('SELECT *  FROM article ORDER BY created_at DESC ');
$popular = $bdd->query('SELECT *  FROM article ORDER BY created_at DESC LIMIT 0,3 ');
$rep =  $bdd->query('SELECT *, count(*) as nombre FROM article GROUP BY categorie');
$donnees = $reponse->fetch();
              $id = $donnees['id'];
if(isset($_GET['categorie'])){
  $nombreTotal = $rep->fetch();
  $num = $nombreTotal['nombre'];
}
else{
  $count = $bdd->query('SELECT count(id) AS num FROM article');
  $nombreTotal = $count->fetch();
  $num = $nombreTotal['num'];
}

$perPage = 6;
$nbPage = ceil($num/$perPage);
if(isset($_GET['page']) || isset($_GET['categorie']))
{
  
  if( is_numeric($_GET['page']) &&($_GET['page']>0 && $_GET['page']<=$nbPage) )
  {
    $cPage = $_GET['page'];
  }
  else{
    header("Location: error/index.html");
  }
}    
else
{
  $cPage = 1;
}
if(isset($_GET['page']) && isset($_GET['categorie']))
{
  $cate = $_GET['categorie'];
  $reponse = $bdd->query("SELECT *  FROM article WHERE categorie='$cate'  ORDER BY created_at DESC LIMIT ".(($cPage-1)* $perPage).",$perPage");
}
else
{
 $cate = "Blog";
$reponse = $bdd->query("SELECT *  FROM article ORDER BY created_at DESC LIMIT ".(($cPage-1)* $perPage).",$perPage");
}


 ?>
<!doctype.html>
<html="no-js" lang="en">
<head>
<meta name="author" content="name">
<meta name="keywords" content="bobo digital talking">
<meta name="DESCRIPTION" content="a php manual">
<!-- Document Title -->
<title><?php echo $cate?> | Dev Corp</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<!-- Favicon -->
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
<link rel="icon" href="images/favicon.ico" type="image/x-icon">

<!-- FontsOnline -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Alegreya+Sans:400,500,700,800,900,300,100' rel='stylesheet' type='text/css'>

<!-- StyleSheets -->
<link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<link rel="stylesheet" href="css/bootstrap/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/responsive.css">
<!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
<link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />
<!-- JavaScripts -->
<script src="js/vendors/modernizr.js"></script>
<!--.php5 Shim and Respond.js IE8 support of.php5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com.php5shiv/3.7.2.php5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
.pagination {
  display: flex;
    justify-content: center;
}

.pagination a {
    color: black;
    float: left;
    padding: 8px 16px;
    text-decoration: none;
}

.pagination a.active {
    color: white;
    background-color: #3498db;
}

.pagination a:hover:not(.active) {background-color: #fff;}
</style>
</head>
<body>
<!-- LOADER ===========================================-->
  <div class="loader">
    <div class="position-center-center">
      <div id="preloader6"> <span></span> <span></span> <span></span> <span></span> </div>
    </div>
  </div>
<div id="loader">
</div>

<!-- Page Wrapper -->
<div id="wrap"> 
  
  <!-- Top bar -->
   <div class="container">
    <div class="row">
      <div class="col-md-2 noo-res"></div>
      <div class="col-md-10">
        <div class="top-bar">
          <div class="col-md-3">
            <ul class="social_icons">
              <li><a href="https://www.facebook.com/Digital.DevCorp" target="_blank"><i class="fa fa-facebook"></i></a></li>
              <li><a href="https://twitter.com/DevC4ASM" target="_blank"><i class="fa fa-twitter"></i></a></li>
              <li><a href="https://plus.google.com/u/0/+DevCorpFourASM" target="_blank"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="https://www.linkedin.com/in/devcorp/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
            </ul>
          </div>
          
          <!-- Social Icon -->
          <div class="col-md-9">
            <ul class="some-info font-montserrat">
              <li><i class="fa fa-phone"></i> +221 76 660 30 10</li>
              <li><i class="fa fa-envelope"></i> contact@devcorptechnologies.com</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Header -->
  <header class="header coporate-header">
    <div class="sticky">
      <div class="container">
        <div class="logo"> <a href="index.php"><img src="images/logo.png" alt=""></a> </div>
        
        <!-- Nav -->
        <nav>
          <ul id="ownmenu" class="ownmenu">
            <li><a href="index.php">HOME</a></li>
            <li><a href="index.php#about"> ABOUT </a></li>
            <li><a href="index.php#services"> SERVICES </a></li>
           <li><a href="index.php#references"> REFERENCES </a></li>
            <li  class="active"><a href="blog.php"> BLOG </a></li>
            <li><a href="#contact"> CONTACT</a></li>
            
            <!--======= SEARCH ICON =========-->
            <li class="search-nav right"><a href="#."><i class="fa fa-search"></i></a>
              <ul class="dropdown">
                <li>
                  <form>
                    <input type="search" class="form-control" placeholder="Enter Your Keywords..." required>
                    <button type="submit"> SEARCH </button>
                  </form>
                </li>
              </ul>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  </header>
  <!-- End Header --> 
  
  <!--======= SUB BANNER =========-->
  <section class="sub-banner">
    <div class="container">
      <div class="position-center-center">
        <h2><?php echo $cate;?></h2>
        <ul class="breadcrumb">
          <li><a href="#.">Home</a></li>
          <li>Blog</li>
        </ul>
      </div>
    </div>
  </section>
  
  <!-- Content -->
  <div id="content"> 
    
    <!-- Latest News -->
    <section class="latest-news blog padding-top-100 padding-bottom-100">
      <div class="container"> 
        
        <!-- Blog Side -->
        <div class="row">
          <div class="col-md-9"> 
             <?php
            while ($donnees = $reponse->fetch()){
              $id = $donnees['id'];
           ?>
            <!-- News 1 -->
            <article class="margin-bottom-50"> <a href="blog-single.php?id=<?php echo $donnees['id']?>  "> <img class="img-responsive" src="DevCorpDashBoard/uploads/<?php echo $donnees['image']; ?>" alt=""> </a>
              <div class="news-detail">
                <div class="row">
                  <div class="col-md-3 text-center">
                    <div class="avatar"><img class="img-circle" src="images/avatar-1.png" alt=""> </div>
                    <p><?php echo $donnees['created_at']?></p>
                    <p><i class="fa fa-comment"></i>
                    <?php $result= $bdd->query(" SELECT count(*) as numb FROM comment WHERE idArticle= $id");
                          $comment = $result->fetch();$com = $comment['numb']; echo $com;
                     ?></p>
                  </div>
                  <div class="col-md-9"> <a href="blog-single.php?id=<?php echo $donnees['id']?>"><?php echo $donnees['title']?></a>
                    <p><?php echo substr($donnees['content'],0,600);?></p>
                  </div>
                </div>
              </div>
            </article>
            <?php }?>
<div class="pagination">
  <a href="#">&laquo;</a>
  <?php 
  for ($i=1;$i<=$nbPage;$i++)
  {
      if($i == $cPage)
      {?>
     <a  class ="active" href="blog.php?page=<?php echo $i;?> "><?php echo $i;?></a>
 
    <?php  }else {
  ?>
  <a  href="blog.php?page=<?php echo $i;?>"><?php echo $i;}}?></a>
  
  <a href="#">&raquo;</a>
</div>
          </div>          
          <!-- Side Bar -->
          <div class="col-md-3">
            <div class="side-bar"> 
              
              <!-- Categories -->
              <h5 class="font-alegreya ">Categories</h5>
              <ul class="cate bg-defult">
                 <?php
            while ($cat = $rep->fetch()){
           ?>
                <li><a href="blog.php?page=1&categorie=<?php echo $cat['categorie'];?>"><?php echo $cat['categorie'];?> <span><?php echo $cat['nombre'];?></span></a></li>
                <?php
                }?>
                <li><a href="blog.php?page=1"><span>View All <i class="fa fa-long-arrow-right"></i></span></a></li>
              </ul>
              
              <!-- Categories -->
              <h5 class="font-alegreya">Latest Tweets</h5>
              <ul class="tweets bg-defult">
                <li>@Murphy zim To a deluxe apartment
                  in the sky <a href="#.">http://Comr.designer.com </a><span>1 hours ago</span></li>
                <li>@Murphy zim To a deluxe apartment
                  in the sky <a href="#.">http://Comr.designer.com </a><span>1 hours ago</span></li>
              </ul>
              
              <!-- Popular Post -->
              <h5 class="font-alegreya">Popular Post</h5>
              <div class="papu-post margin-t-40">
                <ul class="bg-defult">
                  <?php
            while ($pop = $popular->fetch()){
           ?>
                  <li class="media">
                    <div class="media-left"> <a href=""blog-single.php?id=<?php echo $pop['id']?>""> <img class="media-object" src="DevCorpDashBoard/uploads/<?php echo $pop['image']; ?>" alt=""></a> </div>
                    <div class="media-body"> <a class="media-heading" href=""blog-single.php?id=<?php echo $pop['id']?>""><?php echo $pop['title']; ?></a> <span><?php echo $pop['created_at']; ?></span> </div>
                  </li>
                  <?php
                  }?>
                  
                </ul>
              </div>
              
              <!-- Categories -->
             
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- End Content --> 
  
  <!-- Footer -->
<footer>
    <div class="container">
      <div class="row">
        
        
        <!-- Folow Us -->
        <div class="col-md-12 padding-top-50">
          <div class="news-letter">
            <h6>Suivez-nous sur:</h6>
            <ul class="social_icons pull-left margin-left-50 margin-top-10">
              <li><a href="https://www.facebook.com/Digital.DevCorp" target="_blank"><i class="fa fa-facebook"></i></a></li>
              <li><a href="https://twitter.com/DevC4ASM" target="_blank"><i class="fa fa-twitter"></i></a></li>
              <li><a href="https://plus.google.com/u/0/+DevCorpFourASM" target="_blank"><i class="fa fa-google-plus"></i></a></li>
              <li><a href="https://www.linkedin.com/in/devcorp/" target="_blank"><i class="fa fa-linkedin"></i></a></li>
              
            </ul>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Footer Info -->
    <div class="footer-info">
      <div class="container">
        <div class="row"> 
          
          <!-- About -->
          <div class="col-md-4" id="contact"> <img class="margin-bottom-30" src="images/logo-footer.png" alt="" >
            <p>DEV CORP est une compagnie qui évolue dans le domaine des TICs en tant que fournisseur de services et de solutions informatiques. L'équipe a été constituée sur les bases de l'esprit groupe, la compétence, l’exigence en terme de qualité et la volonté d’entreprendre .En somme une équipe jeune, dynamique, qualifié, pleine d’inspiration et de créativité.</p>
            <ul class="personal-info">
              <li><i class="fa fa-map-marker"></i> 9, cité Asecna Batterie Yoff Aéroport</li>
              <li><i class="fa fa-envelope"></i> contact@devcorptechnologies.com</li>
			  <li><i class="fa fa-envelope"></i> ahmed.fall@devcorptechnologies.com</li>
			  <li><i class="fa fa-envelope"></i> ahmadou.fall@devcorptechnologies.com</li>
              <li><i class="fa fa-phone"></i> +221 76 660 30 10 </li>
			  <li><i class="fa fa-phone"></i> +221 77 306 77 19 </li>
            </ul>
          </div>         
          <!-- Service provided -->
          <div class="col-md-4">
            <h6>Envoyez nousun message</h6>
            <div class="quote">
              <form method="POST" action="message.php">
                <input class="form-control" name="nom" type="text" placeholder="Name">
                <input class="form-control" name="mail" type="email" placeholder="Email">
                <textarea class="form-control" name="message" placeholder="Messages"></textarea>
                <button type="submit" class="btn btn-orange">SEND NOW</button>
              </form>
            </div>
          </div>          
          <!-- Quote -->
          <div class="col-md-4">
            <h6>Facebook</h6>            
              <div class="fb-page" data-href="https://www.facebook.com/Digital.DevCorp" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/Digital.DevCorp" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/Digital.DevCorp">DEV CORP</a></blockquote></div>            
          </div>
        </div>
      </div>
    </div>   
    <!-- Rights -->
    <div class="rights">
      <div class="container">
        <p>Copyright © 2016 DEV CORP. All Rights Reserved to DEV CORP TECHNOLOGIES</p>
      </div>
    </div>
  </footer>
</div>
<!-- End Page Wrapper --> 

<!-- JavaScripts --> 
<script src="js/vendors/jquery/jquery.min.js"></script> 
<script src="js/vendors/bootstrap.min.js"></script> 
<script src="js/vendors/own-menu.js"></script> 
<script src="js/vendors/flexslider/jquery.flexslider-min.js"></script> 
<script src="js/vendors/jquery.isotope.min.js"></script> 
<script src="js/vendors/owl.carousel.min.js"></script> 
<script src="js/vendors/jquery.sticky.js"></script> 

<!-- SLIDER REVOLUTION 4.x SCRIPTS  --> 
<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script> 
<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script> 
<script src="js/main.js"></script>
</body>
</html>